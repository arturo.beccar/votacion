
async function main () {
  
    const address = "0x1743E5e00AAe6dB00BBf9F4E52D09fE01CE40c40";
    const Votacion = await ethers.getContractFactory('Votacion');
    const votacion = await Votacion.attach(address);


    // Presidente
    const presidente = await votacion.presidente();
    console.log('El Presidente es:', presidente);


    // Numeros de Votos
    console.log("Numero de Votos:")
    const address1 = "0xA18aDAfa7c870629F0740e8FdF0c29029ccE1EA4";
    console.log(address1, "tiene", await votacion.obtenerNumeroDeVotos(address1), "votos" )
    console.log(presidente, "tiene", await votacion.obtenerNumeroDeVotos(presidente), "votos" )
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });


