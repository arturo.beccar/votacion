import React from "react";

export function Votar({ votar }) {
  return (
    <div>
      <h4>Votar</h4>
      <form
        onSubmit={(event) => {
          // This function just calls the VotarTokens callback with the
          // form's data.
          event.preventDefault();

          const formData = new FormData(event.target);
          const candidato = formData.get("candidato");

          if (candidato) {
            votar(candidato);
          }
        }}
      >
        <div className="form-group">
          <input className="btn btn-primary" type="submit" value="Votar" />
        </div>
      </form>
    </div>
  );
}
