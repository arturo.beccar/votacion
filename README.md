# Votacion

Implementacion del ejercicio "Create and Deploy a Basic Smart Contract and Interact with It from MetaMask" de la classroom https://classroom.google.com/c/NDYwMDk0NTkyMDAz/a/NDYwMDk2ODUyMzU0/details 

En el contrato Votacion, hay un presidente que comienza siendo el address que deployo el contrato.

Cualquier persona puede despues votar a un candidato (alguna address), si el numero de votos de una address supera al del presidente, esa nueva address se vuelve presidente.

## Despliegue

1. En la carpeta del proyecto (cd votacion), correr en la terminal:

npm install

npx hardhat node

En otra terminal correr:

npx hardhat --network localhost run scripts/deploy.js

2. En la carpeta de frontend, correr en la terminal:

npm install

npm run start

3. Abrir el app en la url http://localhost:3000/ , asegurarse de conectarse con metamask al localhost:8545 (Settings -> Networks -> Localhost 8545) y poner Chain ID input en 31337.

## Project status

Ya pude conectar el app a metamask y correr el frontend.

Estoy debuggeando el boton Votar.
