
require("@nomiclabs/hardhat-waffle");

const API_URL = "https://rinkeby.infura.io/v3/a8e3c359deb447d29e7f92b00ab80cc5";

const PRIVATE_KEY = "26b762461dbb7f94f6136ec7d1c7d4a3141741d36380dd08e3d937fcb886734d";


module.exports = {
  solidity: "0.7.0",
  networks: {
    rinkeby: {
      url: API_URL,
      accounts: [PRIVATE_KEY]
    }
  }
};